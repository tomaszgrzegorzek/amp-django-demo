

# Requirements:
  0. Python 3.8
  1. Django
  2. amp package installed from submodule

To avoid 

```console
AttributeError: '_thread._local' object has no attribute 'value'
```

run django with
```bash
python manage.py runserver --nothreading --noreload
```
